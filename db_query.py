import traceback

from db_connection import cursor, conn


def query_generator(json_input):
    # Map operators to SQL conditions
    operator_mapping = {
        "contains": "LIKE",
        "does_not_contain": "NOT LIKE",
        "equals": "=",
        "does_not_equal": "!=",
        "is_less_than": "<",
        "is_greater_than": ">",
    }

    field_mapping = {
        "from": "sender",
        "to": "receiver",
        "subject": "subject",
        "body": "body",
        "date_received": "date",
    }

    # Parse JSON input
    conditions = json_input.get("conditions", {})
    match_conditions = json_input.get("match_conditions", "all").lower()

    # Initialize query components
    select_clause = "SELECT email_id FROM email_data"
    where_conditions = []
    value_placeholders = ()

    # Generate WHERE conditions based on JSON input
    for field, rules in conditions.items():
        sql_field = field_mapping.get(field)
        for rule in rules:
            operator, value = list(rule.items())[0]
            sql_operator = operator_mapping.get(operator)
            if sql_operator:
                # Handle special case for date_received
                if sql_field == "date" and operator == "is_less_than" or operator == "is_greater_than":
                    int_value, unit = value.split()
                    date_condition = f"{sql_field} {sql_operator} strftime('%Y-%m-%d %H:%M:%S', 'now', '-{int_value} {unit}')"
                    where_conditions.append(date_condition)
                else:
                    where_conditions.append(f"{sql_field} {sql_operator} ?")
                    if sql_operator == "LIKE":
                        value_placeholders += (f"%{value}%",)
                    else:
                        value_placeholders += (value,)

    # Combine WHERE conditions based on match_conditions
    if match_conditions == "all":
        combined_conditions = " AND ".join(where_conditions)
    else:
        combined_conditions = " OR ".join(where_conditions)

    # Combine the final query
    final_query = f"{select_clause} WHERE {combined_conditions}"

    return final_query, value_placeholders


async def save_to_db(email_id, email_body, sender, receiver, subject, date):
    try:
        query = ("INSERT INTO email_data ("
                 "email_id, body, sender, receiver, subject, date) "
                 "VALUES (?, ?, ?, ?, ?, ?)")
        values = (email_id, email_body, sender, receiver, subject, date)

        cursor.execute(query, values)
        conn.commit()

    except Exception as e:
        print("error inserting emails", str(e))
        print("Traceback:", traceback.format_exc())


def filter_emails_by_rules(json_input):
    try:
        query, values = query_generator(json_input)
        results = cursor.execute(query, values).fetchall()
        return results
    except Exception as e:
        print("error filtering emails", str(e))
        print("Traceback:", traceback.format_exc())
        return []
