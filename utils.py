import re


def extract_email(sender_value):
    # Define a regular expression pattern to match email addresses
    email_pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

    # Use re.findall to find all matches in the sender_value
    matches = re.findall(email_pattern, sender_value)

    # If matches are found, return the first match, otherwise return None
    return matches[0] if matches else None
