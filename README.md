## Assignment

### Simple project to use gmail API to read email and save it to database and perform certain action on saved data.

## Steps to Install and Run the project:
create a virtual environment and activate it.

### Clone the project
```shell
git clone git@gitlab.com:aslirajesh/gmailapiintegrations.git
```

### Install requirements
```shell
pip install -r requirements.txt
```

## For fetching the email and save it to database 
```shell
python fetch_mail.py
```
It will prompt you to login to your gmail account and allow the app to access your gmail account. After that it will fetch the email and save it to database.
## Since the this APP is not verified by google, 
* you will get a warning message while login to your gmail account. You can click on advance and proceed to login.
* It will work only for email id which are added in google console.
### Now the email will be saved to database and you can perform certain action on it.

#### Edit or add rule in rules.json file to perform certain action on the email.

Now run the below command to perform action on the email.
```shell
python process_mail.py <rule_name>
```
