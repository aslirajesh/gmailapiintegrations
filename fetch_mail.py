import asyncio
from concurrent.futures import ThreadPoolExecutor

from dateutil import parser
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

from authenticate import authenticate
from db_connection import cursor, conn
from db_query import save_to_db
from utils import extract_email


# create db if not exist
cursor.execute("CREATE TABLE IF NOT EXISTS email_data "
               "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
               "email_id VARCHAR, body TEXT, sender VARCHAR, "
               "receiver VARCHAR, subject VARCHAR, date TIMESTAMP)")
conn.commit()


async def load_emails(msg_id, creds):
    try:
        service = build('gmail', 'v1', credentials=creds)

        def load_email_data():
            message = service.users().messages().get(
                userId='me',
                id=msg_id,
                format='full').execute()
            headers = message['payload']['headers']
            # print(headers)
            # print(message)

            def get_header_value(name):
                return next((header['value'] for header in headers
                             if header['name'] == name), None)

            email_id = message['id']
            email_body = message['snippet']
            sender_value = get_header_value('From')
            sender = extract_email(sender_value)
            receiver_value = get_header_value('To')
            receiver = extract_email(receiver_value)
            subject = get_header_value('Subject')
            date = get_header_value('Date')
            parsed_date = parser.parse(date)
            formatted_date = parsed_date.strftime("%Y-%m-%d %H:%M:%S")

            return {
                "email_id": email_id,
                "email_body": email_body,
                "sender": sender,
                "receiver": receiver,
                "subject": subject,
                "date": formatted_date,
            }

        with ThreadPoolExecutor() as executor:
            response = await asyncio.get_event_loop().run_in_executor(
                executor, load_email_data)

        # print(response)
        await save_to_db(
            response['email_id'],
            response['email_body'],
            response['sender'],
            response['receiver'],
            response['subject'],
            response['date']
        )

    except HttpError as e:
        # Handle Google API errors
        print(f"Google API Error: {str(e)}")
    except Exception as e:
        print(f"Error fetching email details: {str(e)}")


async def main():
    # gmail api service
    try:
        creds = authenticate()
        service = build('gmail', 'v1', credentials=creds)
        print("Fetching all emails...")
        messages = []
        page_token = None

        while True:
            results = service.users().messages().list(
                userId='me',
                maxResults=500,
                pageToken=page_token
            ).execute()

            messages.extend(results.get('messages', []))
            page_token = results.get('nextPageToken')

            if not page_token:
                break
        if messages:
            print("loading email and saving to local db " + str(len(messages)))
            tasks = [load_emails(message['id'], creds) for message in messages]
            await asyncio.gather(*tasks)
            print("All emails loaded and saved to local db")
        else:
            print('Inbox is empty')
    except HttpError as error:
        print(f'Error occurred wile fetching emails {error}')


if __name__ == '__main__':
    asyncio.run(main())
