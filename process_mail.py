import asyncio
import functools
import json
import sys
import traceback

from googleapiclient.discovery import build

from authenticate import authenticate
from db_query import filter_emails_by_rules


async def get_labels(service):
    return service.users().labels().list(userId='me').execute()


def get_label_id(service, folder_name):
    try:
        labels = service.users().labels().list(userId='me').execute()
        for label in labels['labels']:
            if label['name'] == folder_name:
                return label['id']
        return None
    except Exception as e:
        print(f"Error fetching label id: {str(e)}")
        traceback.print_exc()


async def mark_as_read(service, email_id):
    try:
        service.users().messages().modify(
            userId='me',
            id=email_id,
            body={'removeLabelIds': ['UNREAD']}).execute()
    except Exception as e:
        traceback.print_exc()


async def mark_as_unread(service, email_id):
    try:
        service.users().messages().modify(
            userId='me',
            id=email_id,
            body={'addLabelIds': ['UNREAD']}).execute()
    except Exception as e:
        traceback.print_exc()


async def move_to_folder(service, email_id, folder_name):
    try:
        label_id = get_label_id(service, folder_name)
        if label_id:
            service.users().messages().modify(
                userId='me',
                id=email_id,
                body={'addLabelIds': [label_id]}).execute()
        else:
            print(f"Label not found for: {folder_name}")
    except Exception as e:
        print(f"Error in moving email to folder: {str(e)}")


async def take_action_on_mail(rule_json, filtered_mail):
    print(f"Taking action on {len(filtered_mail)} emails based on rule")
    actions = rule_json['actions']
    filtered_mail = [item[0] for item in filtered_mail]
    task = []
    creds = authenticate()
    service = build('gmail', 'v1', credentials=creds)
    for action in actions:
        if action == 'mark_as' and actions[action] == 'READ':
            for mail_id in filtered_mail:
                task.append(mark_as_read(service, mail_id))
        elif action == 'mark_as' and actions[action] == 'UNREAD':
            for mail_id in filtered_mail:
                task.append(mark_as_unread(service, mail_id))
        elif action == 'move_to':
            for mail_id in filtered_mail:
                task.append(move_to_folder(service, mail_id, actions[action]))
        else:
            print(f"Action not found: {action}")
    print("task")
    await asyncio.gather(*task)
    print("Action completed")


async def filter_emails():
    # Check if there are enough arguments
    if len(sys.argv) < 1:
        print("Pass Rule name as argument")
    # Access the arguments
    rule = sys.argv[1]
    with open('rules.json', 'r') as rf:
        rules_json = json.load(rf)
        # Check if the rule exists
    if rule in rules_json:
        filtered_mail = filter_emails_by_rules(rules_json[rule])
        if len(filtered_mail) > 0:
            await take_action_on_mail(rules_json[rule], filtered_mail)
        else:
            print("No emails found for the rule")
    else:
        print("Rule not found")


if __name__ == "__main__":
    asyncio.run(filter_emails())
